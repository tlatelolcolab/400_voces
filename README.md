![image info](imgs/400_voces_portada.png)


## Repositorio de información alrededor del proyecto **400 Voces**

El Diccionario para el ANálisis de la Intensidad y el sEntimiento del Español
LAtinoamericano es un innovador proyecto de ciencia ciudadana del Tlatelolco
LAB del Programa Universitario de Estudios sobre Democracia, Justicia y
Sociedad de la UNAM. Busca qué personas como tú participen en la investigación
científica clasificando palabras para crear un Diccionario que nos ayude a
realizar Análisis de Sentimiento la cuál es una metodología que permite
determinar si un texto escrito expresa emociones positivas, negativas o
neutras.

Este proyecto es pionero en el idioma español y contribuye a la sociedad al
permitir buscar, identificar y combatir, en las redes sociodigitales, fenómenos
como la desinformación, los mensajes de odio, el acoso y aquellos que afectan
la cultura política democrática.
